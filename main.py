from bs4 import BeautifulSoup
import array as arr
import requests
import time
from pathlib import Path

playerList = []
streams = []

radioURL = "http://streaming.narodni.hr/stream/player.html"
filePath = "C:\\Users\\Makky\\AppData\\Local\\Programs\\Python\\Python37\\MusicScraper"
starttime = time.time()
file = Path("C:\\Users\\Makky\\AppData\\Local\\Programs\\Python\\Python37\\MusicScraper\\music.txt")

if file.exists() == False:
    f = open("C:\\Users\\Makky\\AppData\\Local\\Programs\\Python\\Python37\\MusicScraper\\music.txt","w+")
    f.close()

#GET SONGS AND STREAM NAME
def getSongs():
    for s in streams:
        source = requests.get(s+"/index.html?sid=1").text
        soup = BeautifulSoup(source,'lxml')

        for node in soup.find_all('b'):
            if "Narodni" in node.text:
                print("\n"+node.text)
                break
        
        for node in soup.find_all('a'):
            if node.has_attr('href'):
               href = node.get("href")
               if "currentsong" in href:
                   print("\n\t",node.text+"\n")
                   #CHECK IF ALREADY EXISTS
                   if songExists(node.text) == False:
                       f = open("C:\\Users\\Makky\\AppData\\Local\\Programs\\Python\\Python37\\MusicScraper\\music.txt","a")
                       f.write(node.text+"\n")
                       f.close()

#CHECK IF SONG ALREADY EXISTS IN FILE                    
def songExists(song):
    ex = False
    with open(filePath+"\music.txt") as f:
        content = f.readlines()
        for l in content:
            if song in l:
                ex = True
                break
    return ex
    
print("PLAYER LIST:")
#GET PLAYERLIST
source = requests.get(radioURL).text
soup = BeautifulSoup(source,'lxml')
for tag in soup.find_all("a",href=True):
    if "player" in tag['href']:
        playerList.append(tag['href'])
        print("\t"+tag['href'])

print("\nSTREAM LIST:")
              
#GET STREAMS
for player in playerList:
    source = requests.get("http://streaming.narodni.hr/stream/"+player).text
    soup = BeautifulSoup(source,'lxml')
    stream = soup.find('li', class_='xradiostream').text
    strm=stream.replace("/;","")
    streams.append(strm)
    print("\t"+strm)

while True:
    getSongs()
    print("**************** Waiting 1 minute! ****************")
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
