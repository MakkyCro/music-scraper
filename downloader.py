from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

from pathlib import Path
import time
from selenium.webdriver.firefox.options import Options
from os import listdir
from os.path import isfile, join
import difflib
import re
from bs4 import BeautifulSoup

import requests


op = Options()
op.set_preference("browser.download.folderList", 2)
op.set_preference("browser.download.manager.showWhenStarting", False)
op.set_preference("browser.download.dir", "C:\\Users\\Makky\\Desktop\\Music")
op.set_preference("browser.helperApps.neverAsk.saveToDisk", ".mp3 audio/mpeg")

browser = webdriver.Firefox(options=op)

musicList = []
preuzete = []
linkovi_s_youtuba = []

file = Path(
    "C:\\Users\\Makky\\AppData\\Local\\Programs\\Python\\Python37\\MusicScraper\\music.txt")
youtube_lista = ""


def ispis_preuzetih(odabir):
    print("Already downloaded:\n")
    for f in preuzete:
        print(f)

    if odabir == 1:
        f = open(file)
        content = f.readlines()
        print("Music List:\n")
        for m in content:
            print(m.replace("\n", ""))
            musicList.append(m.replace("\n", ""))
        f.close()

        print("\n")
    else:
        page = requests.get(youtube_lista)
        soup = BeautifulSoup(page.text, 'html.parser')
        # print(soup.prettify())
        results = soup.find_all(
            'a[class="yt-simple-endpoint style-scope ytd-playlist-video-renderer"]')
        print(results)


def preuzimanje(odabir):
    if odabir == 1:
        for m in musicList:
            search = m
            search = re.sub(r'[(].*[)].*', "", search)
            if checkIfDownloaded(search) == False:
                search = re.sub(r'-', r' ', search)
                search = re.sub(r' ', r'+', search)
                search = re.sub(r'[+]+', r'+', search)
                browser.get(
                    'https://www.youtube.com/results?search_query='+search)
                # browser.find_element_by_name("search_query").send_keys(m)
                # browser.find_element_by_id("search-icon-legacy").click()
                link = browser.find_element_by_id(
                    "video-title").get_attribute("href")
                print(m+"\n\t"+link)
                browser.get('https://ytmp3.cc')
                # WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID, "theme")))
                # browser.find_element_by_id("theme").click()
                browser.find_element_by_name("video").send_keys(link)
                browser.find_element_by_id("submit").click()
                time.sleep(3)
                WebDriverWait(browser, 12).until(EC.element_to_be_clickable(
                    (By.XPATH, "//a[text()='Download']"))).click()
                # btn_download.click()
                time.sleep(1)
            else:
                print("Already downloaded: " + m + "........ skipping!")

    print("Sleeping for 10 seconds!")
    time.sleep(10)

    browser.close()


def similar(seq1, seq2):
    return difflib.SequenceMatcher(a=seq1.lower(), b=seq2.lower()).ratio() > 0.75


def checkIfDownloaded(songName):
    downloaded = False
    for f in preuzete:
        if similar(songName, f.replace(".mp3", "")):
            # print("Similarity: "+str(difflib.SequenceMatcher(a=songName.lower(), b=f.replace(".mp3","").lower()).ratio()))
            downloaded = True
            break
    return downloaded


odabir = int(input(
    "Unesite 1 za preuzimanje pjesma iz liste u music.txt datoteci ili 2 za preuzimanje iz liste na Youtubu.\nOdabir: "))
if odabir == 2:
    youtube_lista = input(
        "Unesite link za poveznicu do youtube liste.\nLink: ")

if odabir == 1:
    downloadPath = "C:\\Users\\Makky\\Desktop\\Music"
else:
    downloadPath = "C:\\Users\\Makky\\Desktop\\Music_YT_List"


preuzete = [f for f in listdir(downloadPath) if isfile(join(downloadPath, f))]

ispis_preuzetih(odabir)
preuzimanje(odabir)
